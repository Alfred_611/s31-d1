// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// Import the task routes
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;
app.use(express.json()); //to read data from the form
app.use(express.urlencoded({extended:true})); // goes hand-in-hand with top line

// Database connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://dbalfredmanalang:MttlylLvtpD7OACh@wdc028-course-booking.qczdh.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Add the task route
app.use("/tasks", taskRoute)
//localhost:3001/tasks/getall

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));